package com.company.Enums;

import java.io.Serializable;

public enum Direction implements Serializable {
    UP, RIGHT, DOWN, LEFT , NONE;
}
