package com.company.UIs;

public class UIConstants {
    public static final int BUTTON_STARTING_X = 50;
    public static final double BACK_BUTTON_Y_COEFFICIENT = 0.635 ;
    public static final double EXCEPTION_BUTTON_Y_COEFFICIENT = 0.45;
    public static final int MENU_VBOX_STARTING_Y = 160;
    public static final int LABELS_STARTING_X = 40;
    public static final double INFOMENU_STARTING_X =30 ;
    public static final double INFO_LABEL_STARTING_X = 200;
    public static final double ATTACK_STARTING_Y =260 ;
    public static final double ATTACK_STARTING_X = 35;
    public static int DELTA_T = 1000;

    }
